# Changelog

## 0.1 (2020/08/26)
### Added
- Prefix can now be passed as argument to init
- Pre-made help module

### Fixed
- Buggy 'default' cached_property
