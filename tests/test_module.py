from .module_tester import ModuleTester
from frank import Default, Daemon, Command, RegexCommand
from frank.module.exceptions import InvalidCommand
import pytest


def test_property_types():
    """
    Test wether the cached_property's return the expected value
    """

    test_mod = ModuleTester(None)

    assert isinstance(test_mod.default, Default)
    assert hasattr(test_mod.default, 'help_str')

    assert isinstance(test_mod.commands, list)
    assert all((
        isinstance(item, Command) for item in test_mod.commands
    ))

    assert isinstance(test_mod.daemons, list)
    assert all((
        isinstance(item, Daemon) for item in test_mod.daemons
    ))


@pytest.mark.asyncio
async def test_invalid_command():
    test_mod = ModuleTester(None)

    with pytest.raises(InvalidCommand):
        # None is just a placeholder here
        await test_mod('aninvalidcommand', None, None, None)


def test_match():
    test_mod = ModuleTester(None)

    assert test_mod.match('tester')
    assert not test_mod.match('testerrr')


def test_commands_order():
    test_mod = ModuleTester(None)

    types = [Command, RegexCommand, RegexCommand]

    assert all((isinstance(cmd, obj_type)
                for cmd, obj_type in zip(test_mod.commands, types)))
