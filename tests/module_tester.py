from frank import Module, command, default, daemon, regex_command


class ModuleTester(Module):
    PREFIX = 'tester'

    @regex_command('pat')
    async def regex1(pregix, cmd, author, channel, mid):
        pass

    @regex_command('pat2')
    async def regex2(pregix, cmd, author, channel, mid):
        pass

    @command('test')
    async def test(cmd, author, channel, mid):
        pass

    @default()
    async def test2(prefix, cmd, author, channel, mid):
        pass

    @daemon()
    async def test_daemon(self):
        pass
