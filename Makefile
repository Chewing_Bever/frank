# =====CONFIG=====
# Source directory
SRC=frank
# Directory name of the venv
# Don't put spaces in the VENV name, make does not like spaces
# Run make clean first if you change this after already having created a venv
VENV=venv
# Docs directory
DOCS=docs
# Interpreter to create venv with
INTERPRETER=python3.8

all: run 


# Re-create venv when needed
$(VENV)/bin/activate: requirements.txt requirements-dev.txt
	@ echo "Rebuilding venv..."
	@ [ ! -e "$(VENV)" ] || rm -rf "$(VENV)"
	@ "$(INTERPRETER)" -m venv "$(VENV)"
	@ "$(VENV)/bin/pip" install -r requirements.txt -r requirements-dev.txt

build-venv: $(VENV)/bin/activate

# =====CLEANING=====
clean: clean-venv clean-cache clean-docs

# Remove venv
clean-venv:
	@ echo "Removing venv..."
	@ [ ! -e "$(VENV)" ] || rm -rf "$(VENV)"

# Remove cache
clean-cache:
	@ echo "Removing .pyc files..."
	@ find . -type f -name "*.pyc" -delete
	@ echo "Removing caches..."
	@ find . -type d \( -name "__pycache__" -o -name ".pytest_cache" \) -exec rm -r "{}" +

clean-docs:
	@ echo "Removing documentation build..."
	@ [ ! -e "$(DOCS)/build" ] || rm -r "$(DOCS)/build"

clean-setup:
	@ echo 'Removing build artifacts...'
	@ [ ! -e "build" ] || rm -rf build
	@ find . -maxdepth 1 -type d -name '*.egg-info' -exec rm -rf "{}" \;

# =====DOCS=====
docs: build-venv
	@ "$(VENV)/bin/sphinx-apidoc" -o "$(DOCS)/source" "$(SRC)"
	@ "$(VENV)/bin/sphinx-build" "$(DOCS)/source" "$(DOCS)/build"


# =====TESTS=====
test: pytest.ini build-venv
	@ "$(VENV)/bin/pytest" --color=yes


# =====PACKAGING=====
package: README.md LICENSE setup.py test clean-setup
	@ echo "Removing build..."
	@ [ ! -e "dist" ] || rm -r "dist"
	@ echo "Running setup.py..."
	@ "$(VENV)/bin/python" setup.py sdist bdist_wheel

publish: package
	@ [ "$$(git symbolic-ref HEAD --short)" = master ] && { \
		echo 'Publishing to PyPi...'; \
		$(VENV)/bin/python -m twine upload dist/* ; \
	} || { \
		echo 'Publishing to PyPi Testing...'; \
		$(VENV)/bin/python -m twine upload --repository testpypi dist/* ; \
	}


.PHONY: all clean clean-venv clean-cache clean-docs test package docs \
	build-venv run-venv clean-setup
