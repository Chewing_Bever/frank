<!--- Before submitting a request, check if it hasn't already been submitted by someone else. --->

### The idea

<!--- Describe your idea. Does it fix a certain issue? Does it add new functionality? --->

### Intended users

<!--- Who will be using this new feature? --->

### Proposal

<!--- How do you want to fix this issue? You can give some code snippets, or describe a solution. --->

### Further details

<!--- Here, you can add example use cases, a list of benefits, or any other details that will help us tackle the 
issue. --->


/label ~feature
